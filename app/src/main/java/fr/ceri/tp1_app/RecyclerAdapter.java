package fr.ceri.tp1_app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import fr.ceri.tp1_app.data.Country;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private AppCompatActivity context;

    private String[] p = {"France",
            "Allemagne",
            "Espagne",
            "Afrique du Sud",
            "États-Unis",
            "Japon"};

    private String[] c = {"Paris",
            "Berlin", "Madrid",
            "Pretoria", "Washington",
            "Tokyo"};

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemTitle.setText(p[i]);
        viewHolder.itemDetail.setText(c[i]);
        //  viewHolder.itemImage.setImageResource(img[i]);

        String uri = Country.countries[i].getImgUri();
        Context c = viewHolder.itemImage.getContext();
        viewHolder.itemImage.setImageDrawable(c.getResources().getDrawable(c.getResources().getIdentifier (uri, null , c.getPackageName())));
    }

    @Override
    public int getItemCount() {
        return p.length;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView itemImage;
        TextView itemTitle;
        TextView itemDetail;

        ViewHolder(View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.image_flag);
            itemTitle = itemView.findViewById(R.id.nom_pays);
            itemDetail = itemView.findViewById(R.id.cap_pays);

            int position = getAdapterPosition();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                    int position = getAdapterPosition();
                    itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int position = getAdapterPosition();
                            ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment();
                            action.setCountryId(position);
                            Navigation.findNavController(v).navigate(action);
                        }
                    });
                    /* Snackbar.make(v, "Click detected on chapter " + (position+1),
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                     */
                    //// Implementation with bundle
                    // Bundle bundle = new Bundle();
                    // bundle.putInt("numChapter", position);
                    // Navigation.findNavController(v).navigate(R.id.action_FirstFragment_to_SecondFragment, bundle);

                   /* FirstFragmentDirections.ActionFirstFragmentToSecondFragment action = FirstFragmentDirections.actionFirstFragmentToSecondFragment();
                    action.setKeyChapterId(position);
                    Navigation.findNavController(v).navigate(action);*/
                }
            });

            }
        }
}