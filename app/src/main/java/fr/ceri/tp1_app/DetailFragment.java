package fr.ceri.tp1_app;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import fr.ceri.tp1_app.data.Country;

public class DetailFragment extends Fragment {

    Country[] countries = Country.countries;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int id_country = 0;

        TextView countryNameText = view.findViewById(R.id.nom_pays);
        ImageView imageView = view.findViewById(R.id.image_flag);
        EditText cap = view.findViewById(R.id.cap_pays);
        EditText lang = view.findViewById(R.id.langue);
        EditText monn = view.findViewById(R.id.monnaie);
        EditText pop = view.findViewById(R.id.population);
        EditText sup= view.findViewById(R.id.superficie);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());

        id_country = args.getCountryId();

        countryNameText.setText(countries[id_country].getName());
        cap.setText(countries[id_country].getCapital());
        lang.setText(countries[id_country].getLanguage());
        monn.setText(countries[id_country].getCurrency());
        pop.setText(Integer.toString(countries[id_country].getPopulation()));
        sup.setText(Integer.toString(countries[id_country].getArea()));

        String uri = countries[id_country].getImgUri();
        Context c = imageView.getContext();
        imageView.setImageDrawable(c.getResources().getDrawable(c.getResources(). getIdentifier (uri , null , c.getPackageName())));

        view.findViewById(R.id.retour).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}